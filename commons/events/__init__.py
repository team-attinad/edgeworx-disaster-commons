import abc

from .registry import EventRegistry


class BaseEvent(abc.ABC):
    @staticmethod
    @abc.abstractmethod
    def load(data: dict):
        pass
