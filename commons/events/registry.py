from ..observer import Publisher
from ..utils import Singleton


class EventRegistry(Publisher, metaclass=Singleton):
    pass
