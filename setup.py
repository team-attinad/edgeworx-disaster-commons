from setuptools import setup

# TODO Change module name, add all the other dependencies of all MS
setup(
    name='commons',
    version='2.0',
    description='Commons module to house reusable code for EdgeWorx',
    url='https://hrisheekeshr@bitbucket.org/team-attinad/edgeworx-disaster-commons',
    author='Attinad Software',
    author_email='attinad@attinadsoftware.com',
    license='MIT',
    install_requires=[
        'flask',
        'Flask-Cors',
        'mongoengine',
        'flask-mongoengine',
        'Flask-API',
        'gevent',
        'requests'
    ],
    packages=['commons', 'commons.exceptions', 'commons.cleaner', 'commons.events', 'commons.observer'],
    zip_safe=False
)
