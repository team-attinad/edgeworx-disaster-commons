from flask_api.status import *

from commons.logger import Logger


class CommonException(Exception):

    def __init__(self, message, status_code, error_details=None):
        self.message = message
        self.status_code = status_code
        self.error_details = error_details
        Logger.critical("Error {} :{} :{}".format(self.status_code, self.message, self.error_details))

    def to_dict(self):
        response = dict()
        response['status'] = 'Error'
        response['message'] = self.message
        return response


class BadRequest(CommonException):
    def __init__(self, message, error_details=None):
        super().__init__(message=message,
                         status_code=HTTP_400_BAD_REQUEST,
                         error_details=error_details)


class RecordNotFound(CommonException):
    def __init__(self, message, error_details=None):
        super().__init__(message=message,
                         status_code=HTTP_404_NOT_FOUND,
                         error_details=error_details)


class ServerError(CommonException):
    def __init__(self, message, error_details=None):
        super().__init__(message=message,
                         status_code=HTTP_500_INTERNAL_SERVER_ERROR,
                         error_details=error_details)


class MethodNotAllowed(CommonException):
    def __init__(self, message, error_details=None):
        super().__init__(message=message,
                         status_code=HTTP_405_METHOD_NOT_ALLOWED,
                         error_details=error_details)


class UnsupportedMediaType(CommonException):
    def __init__(self, message, error_details=None):
        super().__init__(message=message,
                         status_code=HTTP_415_UNSUPPORTED_MEDIA_TYPE,
                         error_details=error_details)
