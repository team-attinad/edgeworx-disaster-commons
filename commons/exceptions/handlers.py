from flask import Blueprint
from flask.json import jsonify
from flask_api.status import HTTP_405_METHOD_NOT_ALLOWED

from .exceptions import *
from .messages import *

exception_bp = Blueprint('exception', __name__)


@exception_bp.app_errorhandler(CommonException)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@exception_bp.app_errorhandler(HTTP_404_NOT_FOUND)
def not_found_exception(error):
    response_dict = CommonException(PAGE_NOT_FOUND, HTTP_404_NOT_FOUND).to_dict()
    response = jsonify(response_dict)
    response.status_code = HTTP_404_NOT_FOUND
    return response


@exception_bp.app_errorhandler(HTTP_500_INTERNAL_SERVER_ERROR)
def handle_server_error(error):
    response_dict = CommonException(SERVER_ERROR_MESSAGE, HTTP_500_INTERNAL_SERVER_ERROR).to_dict()
    response = jsonify(response_dict)
    response.status_code = HTTP_500_INTERNAL_SERVER_ERROR
    return response


@exception_bp.app_errorhandler(HTTP_405_METHOD_NOT_ALLOWED)
def handle_server_error(error):
    response_dict = CommonException(METHOD_NOT_ALLOWED_MESSAGE, HTTP_405_METHOD_NOT_ALLOWED).to_dict()
    response = jsonify(response_dict)
    response.status_code = HTTP_405_METHOD_NOT_ALLOWED
    return response


@exception_bp.app_errorhandler(HTTP_415_UNSUPPORTED_MEDIA_TYPE)
def handle_unsupported_media_type_error(error):
    response_dict = CommonException(METHOD_NOT_ALLOWED_MESSAGE, HTTP_405_METHOD_NOT_ALLOWED).to_dict()
    response = jsonify(response_dict)
    response.status_code = HTTP_415_UNSUPPORTED_MEDIA_TYPE
    return response
