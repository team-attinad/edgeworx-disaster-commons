SERVER_ERROR_MESSAGE = "Oops!! Something went wrong. Kindly check the logs"
METHOD_NOT_ALLOWED_MESSAGE = "Method not allowed!!"
PAGE_NOT_FOUND = "Sorry!! The requested URL is not found"
