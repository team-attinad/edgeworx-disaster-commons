# Image service keys

IMAGE_ID_KEY = 'image_id'

PREDICTION_KEY = 'prediction'
BOTTOM_RIGHT_KEY = 'bottomright'
TOP_LEFT_KEY = 'topleft'
X_KEY = 'x'
Y_KEY = 'y'
LABEL_KEY = 'label'
CONFIDENCE_KEY = 'confidence'
STATUS_KEY = 'status'
FAILED_COUNT_KEY = 'fail_count'
DETECTED_AT_KEY = 'detected_at'

# Video service keys
VIDEO_ID_KEY = 'video_id'
PROCESSED_KEY = "processed"

# Common keys
TIMESTAMP_KEY = 'timestamp'
RESOLUTION_KEY = 'resolution'
DEVICE_ID_KEY = 'device_id'
USER_ID_KEY = 'user_id'
LATITUDE_KEY = 'latitude'
LONGITUDE_KEY = 'longitude'
PATH_KEY = 'path'
DATE_KEY = 'date'
START_DATE_KEY = 'start_date'
CAMERA_STATUS = 'camera_status'
RELATIVE_PATH_KEY = 'relative_path'


PAGE_NUMBER_KEY = 'page_number'
PAGE_SIZE_KEY = 'page_size'
CREATED_AT_KEY = 'created_at'
UPDATED_AT_KEY = 'updated_at'
END_DATE_KEY = 'end_date'

# FILE KEY
FILE_KEY = 'file'

# KEYS FOR SEARCH PARAMS
LIMIT_KEY = 'limit'
# STATUS CODES
STATUS_UNPROCESSED = 0
STATUS_PROCESSING = 1
STATUS_PROCESSED = 2
STATUS_FAILED = 3

# Devices keys
URL_KEY = "url"
TARGET_DETECTED_KEY = "target_detected"
IS_HIGH_PRIORITY_KEY = "is_high_priority"

# Socket
START = "1111111111111111"
STOP = "0000000000000000"

START_STREAM = 1
STOP_STREAM = 0
START_STREAM_KEY = "start_stream"

# timings
SAVE_TIMINGS = "save_timings"
IMAGE_RECEIVED_AT_KEY = "image_received_at"
IMAGE_SAVED_AT_KEY = "image_saved_at"
IMAGE_WRITTEN_TO_SOCKET_AT_KEY = "image_written_to_socket_at"
IMAGE_ARRIVED_AT_DETECTION_AT_KEY = "image_arrived_at_detection_at"
IMAGE_DETECTION_STARTED_AT_KEY = "image_detection_started_at"
IMAGE_DETECTION_DONE_AT_KEY = "image_detection_done_at"
IMAGE_DETECTION_UPDATED_AT_KEY = "image_detection_updated_at"
IMAGE_COMMAND_UPDATED_AT_KEY = "image_command_updated_at"
IMAGE_COMMAND_PUBLISHED_TO_MQTT_AT_KEY = "image_command_published_to_mqtt_at"
