import logging
import os
from logging import StreamHandler
from logging.handlers import TimedRotatingFileHandler

from commons.logger_constants import *


class Logger:
    rotation_frequency = ROTATION_FREQUENCY.get(os.environ.get("LOG_ROTATION_FREQUENCY",
                                                               ROTATION_FREQUENCY_KEY_DAILY))
    rotation_interval = os.environ.get("LOG_ROTATION_INTERVAL", 1)
    log_backup_count = os.environ.get("LOG_BACKUP_COUNT", 0)

    log_rotation_handler = TimedRotatingFileHandler(when=rotation_frequency,
                                                    interval=rotation_interval,
                                                    backupCount=log_backup_count,
                                                    filename=LOG_FILE_PATH)
    stream_handler = StreamHandler()

    logging.basicConfig(format=LOG_FORMAT,
                        datefmt=LOG_DATE_FORMAT,
                        level=LOG_LEVELS.get(os.environ.get("LOG_LEVEL", NOTSET_KEY)),
                        handlers=[log_rotation_handler, stream_handler])

    @staticmethod
    def info(message):
        logging.info(message)

    @staticmethod
    def debug(message):
        logging.debug(message)

    @staticmethod
    def critical(message):
        logging.critical(message)

    @staticmethod
    def error(message):
        logging.error(message)

    @staticmethod
    def warning(message):
        logging.warning(message)
