import json
import os
import time

import mongoengine

from .logger import Logger


def allowed_file(filename, allowed_extensions):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in allowed_extensions


def create_image_upload_directory(path, upload_folder):
    full_path = os.path.join(upload_folder, path)
    if not os.path.isdir(full_path):
        os.makedirs(full_path)
    return full_path


def valid_id(image_id, allowed_extensions):
    if len(image_id.split("_")) == 3 and allowed_file(image_id, allowed_extensions):
        return True
    return False


def get_path(path, allowed_extensions):
    if not valid_id(path, allowed_extensions):
        raise ValueError("Image Id value format exception")
    if path is "":
        raise ValueError("Image Id cannot be empty")
    path = path.split("_")
    return os.path.join(path[0], path[2].split(".")[0])


def from_json_string(string):
    return json.loads(string)


def to_json_string(dictionary):
    return json.dumps(dictionary)


def create_app(name, blueprints, config, debug=False):
    from flask import Flask
    from flask_mongoengine import MongoEngine
    from commons.exceptions import exception_bp
    app = Flask(name)
    app.config["DEBUG"] = debug
    if type(config) is str:
        app.config.update(from_json_string(config))
    elif type(config) is dict:
        app.config.update(config)
    else:
        app.config.from_object(config)
    for blueprint in blueprints:
        app.register_blueprint(blueprint)
    app.register_blueprint(exception_bp)
    # CORS(app)
    MongoEngine(app)
    return app


def run_app(app):
    from .config import get_config
    config = get_config(app)
    app.run(host=config.FLASK_HOST, port=config.FLASK_PORT, debug=config.DEBUG)


def delete_files_for(files):
    for file in files:
        delete_file(file)


def delete_file(file):
    try:
        os.remove(file)
    except:
        pass


def timed(function):
    def inner(*args, **kwargs):
        start_time = time.time()
        result = function(*args, **kwargs)
        Logger.debug("Took {} seconds to execute {} ".format(time.time() - start_time, function.__name__))
        return result
    return inner


def query_builder(params, comparators={}, field_names={}):
    query = None
    for key, value in params.items():
        tmp = __query__(field_names.get(key, key), value, comparators.get(key, '__exact'))
        if query is None:
            query = tmp
        else:
            query = query & tmp
    return query


def directory_size_in_gb_for(path):
    conversion = {
        "G": 1,
        "M": 1024,
        "K": 1048576
    }
    import subprocess
    if os.path.isdir(path):
        folder_size = subprocess.check_output(['du', '-sh', path]).split()[0].decode('utf-8')
        folder_size_unit = folder_size[-1]
        folder_size_conversion = conversion[folder_size_unit]
        folder_size = float(folder_size[:-1])
        folder_size /= folder_size_conversion
        return folder_size
    else:
        return -1


def __query__(key, value, operator=''):
    return mongoengine.Q(**{(key + operator): value})


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
