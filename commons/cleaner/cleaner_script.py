import schedule

from commons import utils


def init(interval_in_hours, path_to_folder, folder_size_limit, cleaner_method):
    """
    Initializes the cleaner script
    :param interval_in_hours: interval by which the cleaner should be invoked
    :param path_to_folder: path to the folder the cleaner should monitor
    :param folder_size_limit: the threshold size of the folder after which clean should be initiated
    :param cleaner_method: the reference to the method defines the cleaning strategy
    :return: None
    """
    schedule.every(interval_in_hours).hours.do(clean, path_to_folder, folder_size_limit, cleaner_method)
    while True:
        schedule.run_pending()


def clean(path, limit, cleaner_method):
    size = utils.directory_size_in_gb_for(path)
    if size > limit:
        cleaner_method()
