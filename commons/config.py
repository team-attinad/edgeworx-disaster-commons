from commons import utils


def get_config(current_app=None):
    if current_app is None:
        from flask import current_app
    configuration = Configuration()
    configuration.__dict__ = current_app.config
    return configuration


class Configuration:

    def __init__(self, config_json="{}"):
        self.__dict__ = utils.from_json_string(config_json)
